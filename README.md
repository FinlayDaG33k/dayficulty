# Dayficulty
Paper Plugin to change difficulty for the Das Propaganda Maschine Minecraft server.  

**As of May 13th 2024, this plugin was adopted by [The Mermaid's Rock](https://gitlab.com/the-mermaids-rock/development/dayficulty) and subsequently [combined with others into a single plugin](https://gitlab.com/the-mermaids-rock/development/minecraft-plugin).**
