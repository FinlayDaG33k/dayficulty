package nl.finlaydag33k.dayficulty.listener;

import nl.finlaydag33k.dayficulty.Main;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;

public class CreatureSpawnListener implements Listener {
  @EventHandler
  public void onCreatureSpawn(CreatureSpawnEvent event) {
    // Check if we should spawn today
    if(Main.doSpawn) return;
    
    // Cancel natural spawn events
    if(event.getSpawnReason().equals(CreatureSpawnEvent.SpawnReason.NATURAL)) event.setCancelled(true);
  }
}
