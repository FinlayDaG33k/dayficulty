package nl.finlaydag33k.dayficulty;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Main extends JavaPlugin {
    public static Main plugin;
    public static boolean doSpawn = false;

    @Override
    public void onEnable() {
        Main.plugin = this;
        
        // Update spawn status
        Main.doSpawn = Main.shouldSpawn(new Date());

        // Schedule task to update spawn status each day
        Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
            @Override
            public void run() {
                // Get the current date
                Date now = new Date();

                // Have the daily change be at 5 o'clock
                if(!new SimpleDateFormat("H").format(now).equals("5")) return;
                if(!new SimpleDateFormat("m").format(now).equals("0")) return;

                // Enable of disable spawning based on day
                Main.doSpawn = Main.shouldSpawn(now);
            }
        }, 0, 20 * 60);
    }
    
    public static boolean shouldSpawn(Date now) {
        switch(new SimpleDateFormat("u").format(now)) {
            case "1":
            case "2":
            case "3":
            case "4":
                return false;
            case "6":
            case "5":
            case "7":
                return true;
        }
        
        return false;
    }
}
